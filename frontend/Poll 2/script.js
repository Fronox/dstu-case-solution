 
 const router = new VueRouter({
  routes: [
    { path: '/program/:id' }
  ]
})
 const hed = new Vue({ router }).$mount('#hed')

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'radar',
    data: {
    	labels: ['Актуальность материалов', 'Польза для будущей карьеры ', 'Контакт преподавателя с аудиторией', 'Сложность освоения', 'Возможность расширения кругозора'],
        datasets: [{
            data: [27.92, 17.53, 14.94, 26.62, 12.99],
            backgroundColor: ['#e91e63', '#00e676', '#ff5722', '#1e88e5', '#ffd600'],
            borderWidth: 0.5 ,
            borderColor: ''
        }]
    },
    options: {
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: 'white',
                padding: 15
            }
        },
        tooltips: {
            enabled: false
        },
        plugins: {
            datalabels: {
                color: 'white',
                textAlign: 'center',
                font: {
                    lineHeight: 1.6
                },
                formatter: function(value, ctx) {
                    return ctx.chart.data.labels[ctx.dataIndex] + '\n' + value + '%';
                }
            }
        }
    }
});