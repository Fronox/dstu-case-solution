from typing import List, Dict, Any

from .models import EducationalProgram, Subject
from assessment import models as assessment_models
from django.db.models import Avg, Min, Max, Count
from .serializers import SubjectSerializer


class EducationalProgramService:
    @staticmethod
    def get_grades_stat(program: EducationalProgram, month: str, year: str, subject_serializer: SubjectSerializer):
        def get_stat(subject: Subject) -> Dict[str, Any]:
            grades = assessment_models.Grade.objects \
                .filter(subject=subject, student__program=program)
            if month:
                grades = grades.filter(date__month=month)
            if year:
                grades = grades.filter(date__year=year)

            results_qs = grades \
                .values('type') \
                .annotate(
                    avg=Avg('score'),
                    min=Min('score'),
                    max=Max('score'),
                    total=Count('score')
                )

            results = list(results_qs)
            if len(results) > 0:
                for result in results:
                    result['type'] = assessment_models.Grade.TaskType.CHOICES[int(result['type'])][1]

                return {
                    'subject': subject_serializer.to_representation(subject),
                    'stat': results
                }

        return [get_stat(subject) for subject in program.subjects.all()]
