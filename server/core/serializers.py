from rest_framework import serializers
from .models import Student, Subject, EducationalProgram


class StudentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student
        fields = (
            'url',
            'uuid',
            'name',
            'surname',
            'patronymic',
            'program'
        )

    program = serializers.HyperlinkedRelatedField(
        view_name='educational-program-detail', queryset=EducationalProgram.objects.all()
    )


class SubjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Subject
        fields = (
            'url',
            'uuid',
            'title',
            'code'
        )


class EducationalProgramSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EducationalProgram
        fields = (
            'url',
            'uuid',
            'type',
            'title',
            'creation_year',
            'code',
            'subjects',
            'years_duration',
            'is_active'
        )

    url = serializers.HyperlinkedIdentityField(view_name='educational-program-detail')

    subjects = serializers.HyperlinkedRelatedField(
        view_name='subject-detail', queryset=Subject.objects.all(), many=True
    )
