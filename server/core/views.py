from rest_framework import viewsets, status
from .serializers import StudentSerializer, SubjectSerializer, EducationalProgramSerializer
from .models import Student, Subject, EducationalProgram
from rest_framework.decorators import action
from rest_framework.response import Response
from .services import EducationalProgramService


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class EducationalProgramViewSet(viewsets.ModelViewSet):
    queryset = EducationalProgram.objects.all()
    serializer_class = EducationalProgramSerializer

    @action(methods=['get'], detail=True, url_path='evaluated-grades')
    def get_grades_stat(self, request, pk):
        program: EducationalProgram = self.get_object()
        if not program.is_active:
            return Response(
                data={
                    'message': 'Only subjects from active programs can be evaluated.'
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        month = self.request.query_params.get('month')
        year = self.request.query_params.get('year')

        subject_serializer = SubjectSerializer(context={'request': request})
        grades_stat = EducationalProgramService.get_grades_stat(program, month, year, subject_serializer)
        return Response(
            data={
                'result': [grade_stat for grade_stat in grades_stat if grade_stat]
            }
        )
