import uuid

from django.db import models


class UuidMixin(models.Model):
    class Meta:
        abstract = True

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class Subject(UuidMixin):
    title = models.CharField(max_length=255)
    code = models.CharField(max_length=255)

    def __str__(self):
        return '%s [%s]' % (self.title, self.code)


class EducationalProgram(UuidMixin):
    class ProgramType:
        BACHELOR = 0
        MASTER = 1
        PHD = 2

        CHOICES = (
            (BACHELOR, 'Бакалавриат'),
            (MASTER, 'Магистратура'),
            (PHD, 'Аспирантура')
        )

    type = models.CharField(max_length=15, choices=ProgramType.CHOICES)
    title = models.CharField(max_length=255)
    creation_year = models.SmallIntegerField()
    code = models.CharField(max_length=100)
    subjects = models.ManyToManyField(
        to=Subject, through='SubjectProgramRel'
    )
    years_duration = models.SmallIntegerField()
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return '%s [%s]' % (self.title, self.code)


class Student(UuidMixin):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=100)
    student_id = models.CharField(max_length=255)
    program = models.ForeignKey(
        to=EducationalProgram, on_delete=models.CASCADE
    )

    def __str__(self):
        return '%s %s %s' % (self.surname, self.name, self.patronymic)


class SubjectProgramRel(models.Model):
    subject = models.ForeignKey(
        to=Subject, on_delete=models.CASCADE, null=True
    )

    program = models.ForeignKey(
        to=EducationalProgram, on_delete=models.CASCADE
    )
