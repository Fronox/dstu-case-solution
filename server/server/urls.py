"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from core import views as core_views
from poll import views as poll_views
from assessment import views as assessment_views

router = routers.DefaultRouter()
router.register(r'students', core_views.StudentViewSet, basename='student')
router.register(r'subjects', core_views.SubjectViewSet, basename='subject')
router.register(r'educational-programs', core_views.EducationalProgramViewSet, basename='educational-program')

router.register(r'poll-results', poll_views.PollResultsViewSet, basename='poll-result')
router.register(r'grades', assessment_views.GradeViewSet, basename='grade')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls))
]
