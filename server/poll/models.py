import logging

from django.db import models
from django_fsm import FSMIntegerField
from core import models as core_models
from core.models import UuidMixin

logger = logging.getLogger(__name__)


class PollResults(UuidMixin):
    class Answers:
        AGREE = 5
        SOMEWHAT_AGREE = 4
        NEUTRAL = 3
        SOMEWHAT_DISAGREE = 2
        DISAGREE = 1

        CHOICES = (
            (AGREE, 'Согласен'),
            (SOMEWHAT_AGREE, 'Скорее согласен'),
            (NEUTRAL, 'Нейтрально'),
            (SOMEWHAT_DISAGREE, 'Скорее несогласен'),
            (DISAGREE, 'Несогласен'),
        )

    class Semester:
        AUTUMN = 0
        SPRING = 1

        CHOICES = (
            (AUTUMN, 'Осень'),
            (SPRING, 'Весна')
        )

    title = models.CharField(max_length=255)
    student = models.ForeignKey(
        to=core_models.Student, on_delete=models.CASCADE, related_name='poll_results'
    )
    subject = models.ForeignKey(
        to=core_models.Subject, on_delete=models.CASCADE
    )

    year = models.PositiveSmallIntegerField()
    semester = models.CharField(max_length=5, choices=Semester.CHOICES)

    broadening_horizons = models.CharField(max_length=20, choices=Answers.CHOICES)
    future_career = models.CharField(max_length=20, choices=Answers.CHOICES)
    knowledge_novelty = models.CharField(max_length=20, choices=Answers.CHOICES)
    complexity = models.CharField(max_length=20, choices=Answers.CHOICES)

    understandable_requirements = models.CharField(max_length=20, choices=Answers.CHOICES)
    understandable_materials = models.CharField(max_length=20, choices=Answers.CHOICES)
    lecturer_contact = models.CharField(max_length=20, choices=Answers.CHOICES)
    extra_communication = models.CharField(max_length=20, choices=Answers.CHOICES)
