from rest_framework import viewsets
from django_filters import rest_framework as filters

from .serializers import PollResultsSerializer
from .models import PollResults


class PollResultsViewSet(viewsets.ModelViewSet):
    queryset = PollResults.objects.all()
    serializer_class = PollResultsSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('semester', 'year')
