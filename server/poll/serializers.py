from rest_framework import serializers
from .models import PollResults
from core import models as core_models


class PollResultsSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="poll-result-detail")

    class Meta:
        model = PollResults
        fields = (
            'url',
            'uuid',
            'title',
            'student',
            'subject',
            'semester',
            'year',
            'broadening_horizons',
            'future_career',
            'knowledge_novelty',
            'complexity',
            'understandable_requirements',
            'understandable_materials',
            'lecturer_contact',
            'extra_communication'
        )

    student = serializers.HyperlinkedRelatedField(
        view_name='student-detail',
        queryset=core_models.Student.objects.all()
    )

    subject = serializers.HyperlinkedRelatedField(
        view_name='subject-detail',
        queryset=core_models.Subject.objects.all()
    )
