from rest_framework import serializers
from .models import Grade
from core import models as core_models


class GradeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Grade
        fields = (
            'url',
            'type',
            'student',
            'subject',
            'score',
            'date'
        )

    student = serializers.HyperlinkedRelatedField(
        view_name='student-detail',
        queryset=core_models.Student.objects.all()
    )

    subject = serializers.HyperlinkedRelatedField(
        view_name='subject-detail',
        queryset=core_models.Subject.objects.all()
    )