from django.db import models

from core.models import UuidMixin, Student, Subject


class Grade(UuidMixin):
    class TaskType:
        LAB = 0
        LECTURE = 1
        PROJECT = 2
        EXAM = 3
        PRACTICE = 4
        THESIS = 5

        CHOICES = (
            (LAB, 'Лабораторная'),
            (LECTURE, 'Лекционный контроль'),
            (PROJECT, 'Курсовой проект'),
            (EXAM, 'Экзамен'),
            (PRACTICE, 'Практика'),
            (THESIS, 'ВКР'),
        )

    type = models.CharField(
        max_length=25,
        choices=TaskType.CHOICES
    )

    student = models.ForeignKey(
        to=Student, on_delete=models.CASCADE
    )

    subject = models.ForeignKey(
        to=Subject, on_delete=models.CASCADE
    )

    score = models.SmallIntegerField()

    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return '[%s] %s: %s, %s' % (
            self.date,
            self.student.name,
            self.subject.title,
            self.score
        )
